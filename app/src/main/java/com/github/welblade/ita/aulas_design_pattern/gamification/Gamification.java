package com.github.welblade.ita.aulas_design_pattern.gamification;

public class Gamification {
    private AchievementStorage storage;
    public Gamification(AchievementStorage storage) {
        this.storage = storage;
    }

    public void addPoints(String user, String name, int points) {
        Point point = new Point(name);
        for(var i = 0; i < points; i++){
            storage.addAchievement(user, point);
        }
    }

    public void addBadge(String user, String name) {
        storage.addAchievement(user, new Badge(name));
    }
}
