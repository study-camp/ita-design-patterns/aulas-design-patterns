package com.github.welblade.ita.aulas_design_pattern.gamification;


public class ForumServiceGamificationProxy implements ForumService {
    public ForumServiceGamificationProxy(ForumService service, Gamification gamification) {
        this.service = service;
        this.gamification = gamification;
    }

    private ForumService service;
    private Gamification gamification;

    @Override
    public void addTopic(String user, String topic) {
        service.addTopic(user, topic);
        gamification.addPoints(user, "CREATION", 5);
        gamification.addBadge(user, "I CAN TALK");
    }

    @Override
    public void addComment(String user, String topic, String comment) {
        service.addComment(user, topic, comment);
        gamification.addPoints(user, "PARTICIPATION", 3);
        gamification.addBadge(user, "LET ME ADD");
    }

    @Override
    public void likeTopic(String user, String topic, String topicUser) {
        service.likeTopic(user, topic, topicUser);
        gamification.addPoints(topicUser, "CREATION", 1);
    }

    @Override
    public void likeComment(String user, String topic, String comment, String commentUser) {
        service.likeComment(user, topic, comment,commentUser );
        gamification.addPoints(commentUser, "PARTICIPATION", 1);
    }

    public ForumService getForumService() {
        return service;
    }

    public Gamification getGamification() {
        return gamification;
    }
}