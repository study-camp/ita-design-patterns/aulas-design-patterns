package com.github.welblade.ita.aulas_design_pattern.gamification;

public interface AchievementObserver {
    void achievementUpdate(String user, Achievement a);
}
