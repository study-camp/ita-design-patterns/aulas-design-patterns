package com.github.welblade.ita.aulas_design_pattern.composite;

public interface TrechoAereo {
    public String getOrigem() ;
    public String getDestino() ;
    public int getCusto();
    public int getDistancia() ;
}
