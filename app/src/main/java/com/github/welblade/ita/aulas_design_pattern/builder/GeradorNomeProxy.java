package com.github.welblade.ita.aulas_design_pattern.builder;

public class GeradorNomeProxy extends GeradorNome{
    private GeradorNome gerador;
    private String local;

    public GeradorNomeProxy(GeradorNome gerador, String local) {
        this.gerador = gerador;
        this.local = local;
    }

    public String gerarNome(String nomeBase) {
        return gerador.gerarNome(nomeBase) + " de " + local;
    }

    public void setTratamentoStrategy(Tratamento tratamento) {
        gerador.setTratamentoStrategy(tratamento);
    }

    public Tratamento getTratamentoStrategy() {
        return gerador.getTratamentoStrategy();
    }

    

    


    
}
