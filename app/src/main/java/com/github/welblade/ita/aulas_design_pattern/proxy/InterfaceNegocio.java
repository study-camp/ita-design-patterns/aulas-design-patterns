package com.github.welblade.ita.aulas_design_pattern.proxy;

public interface InterfaceNegocio {

    void executaTransacao();

    void cancelaTransacao();

}
