package com.github.welblade.ita.aulas_design_pattern.gamification;

import java.util.List;

public abstract class Achievement {

    public Achievement(String name){
        this.name = name;
    }

    private String name;

    public String getName(){
        return name;
    }

    public abstract void addTo(List<Achievement> achievements);
}
