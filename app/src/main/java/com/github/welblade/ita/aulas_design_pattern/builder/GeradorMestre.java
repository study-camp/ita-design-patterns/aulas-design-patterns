package com.github.welblade.ita.aulas_design_pattern.builder;

public class GeradorMestre extends GeradorNome {

    @Override
    protected String getTratamento() {
        return "Mestre ";
    }
    
}
