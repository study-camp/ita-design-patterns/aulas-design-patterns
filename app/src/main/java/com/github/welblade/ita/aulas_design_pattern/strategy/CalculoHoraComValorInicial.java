package com.github.welblade.ita.aulas_design_pattern.strategy;

public class CalculoHoraComValorInicial implements Calculo {
    private double valorInicial;
    private int limiteHoraInicial;
    private double valorHoraExcedente;

    public CalculoHoraComValorInicial(
        double valorInicial, 
        int limiteHoraInicial, 
        double valorHoraExcedente
    ) {
        this.valorInicial = valorInicial;
        this.limiteHoraInicial = limiteHoraInicial;
        this.valorHoraExcedente = valorHoraExcedente;
    }

    @Override
    public double calcularTarifa(int qtdHoras) {
        double valor = valorInicial;
        if(qtdHoras > limiteHoraInicial){
            valor += (qtdHoras - limiteHoraInicial) * valorHoraExcedente;
        }
        return valor;
    }

}
