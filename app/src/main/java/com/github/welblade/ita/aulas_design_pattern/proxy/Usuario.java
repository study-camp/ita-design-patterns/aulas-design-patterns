package com.github.welblade.ita.aulas_design_pattern.proxy;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
    
    public Usuario(String nome) {
        this.nome = nome;
        this.permissoes = new ArrayList<>();
    }

    private String nome;

    private List<String> permissoes;
    
    public String getNome() {
        return nome;
    }

    public void autorizaAcesso(String classe, String metodo) {
        permissoes.add(classe + ":" + metodo);
    }

    public boolean verificaAcesso(String classe, String metodo) {
        return permissoes.contains(classe + ":" + metodo);
    }
    

}
