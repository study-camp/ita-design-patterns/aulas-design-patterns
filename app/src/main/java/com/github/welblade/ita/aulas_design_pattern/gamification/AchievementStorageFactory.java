package com.github.welblade.ita.aulas_design_pattern.gamification;

public class AchievementStorageFactory {

    private static AchievementStorage storage;

    public static void setAchievementStorage(AchievementStorage storage) {
        if(AchievementStorageFactory.storage == null){
            AchievementStorageFactory.storage = storage;
        }
    }

    public static AchievementStorage getAchievementStorage() {
        return AchievementStorageFactory.storage;
    }
}
