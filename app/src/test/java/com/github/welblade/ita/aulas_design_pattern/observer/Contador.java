package com.github.welblade.ita.aulas_design_pattern.observer;

public interface Contador {

    int getContagem();

    void contar(String palavra);

}
