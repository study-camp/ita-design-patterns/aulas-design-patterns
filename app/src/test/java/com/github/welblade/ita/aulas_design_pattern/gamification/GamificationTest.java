package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.welblade.ita.aulas_design_pattern.gamification.mock.MockStorage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class GamificationTest {

    @Test
    public void testObjectCreation() {
        MockStorage mock = new MockStorage();
        Gamification gamefication = new Gamification(mock);
        gamefication.addPoints("user", "CREATION", 5);
        gamefication.addBadge("user", "I CAN TALK");
        assertEquals(6, mock.getCalls("addAchievement"));
    }
    
    @Test
    public void testHundredPoints() {
        MemoryAchievementStorage memory = new MemoryAchievementStorage();
        Gamification gamification = new Gamification(memory);
        for(var i = 1; i <= 20; i++){
            gamification.addPoints("user", "CREATION", 5);
        }
        Point point = (Point) memory.getAchievement("user", "CREATION");
        assertEquals(100,point.getPoints()) ;
    }
}
