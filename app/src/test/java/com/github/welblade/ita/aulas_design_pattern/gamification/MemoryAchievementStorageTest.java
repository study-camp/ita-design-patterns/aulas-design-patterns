package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import com.github.welblade.ita.aulas_design_pattern.gamification.mock.MockObserver;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class MemoryAchievementStorageTest {
    @Test
    public void testObjectCreationAndAddOneAchieviment() {
        AchievementStorage storage = new MemoryAchievementStorage();
        Achievement a = new Point("PARTICIPATION");
        storage.addAchievement("user", a);
        assertAll(
            () -> assertTrue(storage.getAchievements("user") instanceof List<?>),
            () -> assertEquals(a.getName(), storage.getAchievement("user", "PARTICIPATION").getName())
        );
    }

    @Test
    public void testAddTwoAchieviments() {
        AchievementStorage storage = new MemoryAchievementStorage();
        Achievement a = new Point("PARTICIPATION");
        Achievement b = new Badge("I can talk!");
        storage.addAchievement("user", a);
        storage.addAchievement("user", b);
        assertAll(
            () -> assertEquals(2, storage.getAchievements("user").size()),
            () -> assertEquals(a.getName(), storage.getAchievement("user", "PARTICIPATION").getName()),
            () -> assertEquals(b.getName(), storage.getAchievement("user", "I can talk!").getName())
        );
    }

    @Test
    public void testAddOneHundredAchieviments() {
        AchievementStorage storage = new MemoryAchievementStorage();
        Achievement a = new Point("PARTICIPATION");

        for(var i = 1; i <= 100; i++){
            storage.addAchievement("user", a);
        }
        Point point = (Point) storage.getAchievement("user", "PARTICIPATION");
        assertAll(
            () -> assertEquals(1, storage.getAchievements("user").size()),
            () -> assertEquals(a.getName(), storage.getAchievement("user", "PARTICIPATION").getName()),
            () -> assertEquals(100, point.getPoints())
        );
    }

    @Test
    public void testTryGetUserThatNotExist() {
        AchievementStorage storage = new MemoryAchievementStorage();
        assertNull(storage.getAchievements("user"));
    }

    @Test
    public void testTryGetAchievementThatNotExist() {
        AchievementStorage storage = new MemoryAchievementStorage();
        Achievement a = new Point("PARTICIPATION");
        storage.addAchievement("user", a);
        assertNull(storage.getAchievement("user", "I can talk!"));
    }

    @Test
    public void testAddObserver() {
        MockObserver observer = new MockObserver();
        AchievementStorage storage = new MemoryAchievementStorage();
        storage.addObserver(observer);
        Achievement a = new Point("PARTICIPATION");
        storage.addAchievement("user", a);
        assertEquals(1, observer.getCalls());
    }
}