package com.github.welblade.ita.aulas_design_pattern.proxy;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class ProxySegurancaTest {

    @Test
    public void testeAutorizaAcesso() {
        Usuario welblade = new Usuario("welblade");
        welblade.autorizaAcesso("InterfaceNegocio", "executaTransacao");
        NegocioMock mock = new NegocioMock();
        InterfaceNegocio n = new SegurançaNegocio(mock, welblade);
        n.executaTransacao();
        assertTrue(mock.foiAcessado());
    }

    @Test
    public void testeSemAcesso() {
        Usuario welblade = new Usuario("welblade");
        welblade.autorizaAcesso("InterfaceNegocio", "executaTransacao");
        NegocioMock mock = new NegocioMock();
        InterfaceNegocio n = new SegurançaNegocio(mock, welblade);

        assertAll(
            () -> assertThrows(RuntimeException.class, ()-> n.cancelaTransacao()),
            () -> assertFalse(mock.foiAcessado())
        );
       
    }
    
}
