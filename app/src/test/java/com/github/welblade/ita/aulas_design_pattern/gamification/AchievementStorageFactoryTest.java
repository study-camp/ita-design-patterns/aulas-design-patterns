package com.github.welblade.ita.aulas_design_pattern.gamification;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class AchievementStorageFactoryTest {
    @Disabled
    @Test
    public void testAchievementStorageCreation() {
        AchievementStorageFactory.setAchievementStorage(new MemoryAchievementStorage());
        AchievementStorage storage = AchievementStorageFactory.getAchievementStorage();
        AchievementStorageFactory.setAchievementStorage(new MemoryAchievementStorage());
        AchievementStorage storage2 = AchievementStorageFactory.getAchievementStorage();
        assertEquals(storage, storage2);
    }
}